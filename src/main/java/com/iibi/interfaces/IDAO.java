package com.iibi.interfaces;

import java.util.List;

public interface IDAO<T> {

	void create(T t);

	void delete(T t);

	void update(T t);

	List<T> getAll(Class<T> classe, String orderBy);

	T find(Class<T> classe, Integer id);

}
