package com.iibi.rest;

import java.net.URISyntaxException;
import com.iibi.dao.PaisDAO;
import com.iibi.interfaces.Secured;
import com.iibi.model.Pais;

import java.util.List;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/paises")
public class WSResource {

    @GET
    @Secured
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pais> getAllPaises() {
        List<Pais> paises = new PaisDAO().getAll(Pais.class, "id");

        return paises;
    }

    @GET
    @Secured
    @Path("/getAllSorted")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pais> getAllPaises(@QueryParam("attributeName") String attributeName) {
        List<Pais> paises = new PaisDAO().getAll(Pais.class, attributeName);
        return paises;
    }

    @POST
    @Secured
    @Path("/add")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response adicionarPais(@FormParam("name") String nome, @FormParam("code") String code)
            throws URISyntaxException {

        if (nome == null || code == null) {
            return Response.status(400).entity("INTRODUZA PELO MENOS O CODIGO e NOME DO PAIS").build();
        }

        PaisDAO dao = new PaisDAO();
        Pais p = new Pais();
        p.setName(nome);
        p.setCode(code);
        dao.create(p);

        return Response.ok().entity("PAIS REGISTADO!").build();

    }

    @PUT
    @Secured
    @Path("/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modificarPais(@FormParam("id") Integer id, @FormParam("name") String nome,
            @FormParam("code") String code) throws URISyntaxException {
        if (id == null) {
            return Response.status(400).entity("ID NECESSARIO").build();
        }

        PaisDAO dao = new PaisDAO();

        Pais p = dao.find(Pais.class, id);

        p.setId(id);

        if (nome != null) {
            p.setName(nome);
        }

        if (code != null) {
            p.setCode(code);
        }

        dao.update(p);

        return Response.ok().entity("MODIFICADO COM SUCESSU!").build();
    }

    @DELETE
    @Secured
    @Path("/remover/{id}")
    public Response removerPais(@PathParam("id") Integer id) throws URISyntaxException {
        if (id == null) {
            return Response.status(400).entity("ID NECESSARIO").build();
        }

        PaisDAO dao = new PaisDAO();
        dao.delete(new Pais(id));

        return Response.status(202).entity("REMOVIDO COM SUCESSU!").build();
    }

}
