
package com.iibi.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.iibi.interfaces.IDAO;
import com.iibi.utils.HibernateUtil;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class DAOGeneric<T> implements IDAO<T> {

	private final Session session;
	private final Transaction transaction;

	public DAOGeneric() {
		this.session = HibernateUtil.getHibernateSession();
		this.transaction = this.session.getTransaction();
	}

	public Session getSession() {
		return session;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	@Override
	public void create(T t) {

		transaction.begin();
		session.save(t);
		transaction.commit();
	}

	@Override
	public void delete(Object t) {
		transaction.begin();
		session.delete(t);
		session.flush();
		transaction.commit();
		session.close();
	}

	@Override
	public void update(T t) {
		transaction.begin();
		session.update(t);
		transaction.commit();
	}

	@Override
	public List<T> getAll(Class<T> classe, String orderBy) {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(classe);
		Root<T> root = query.from(classe);
		query.select(root).orderBy(builder.asc(root.get(orderBy)));

		return session.createQuery(query).getResultList();
	}

	@Override
	public T find(Class<T> classe, Integer id) {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(classe);
		Root<T> root = query.from(classe);
		query.select(root).where(builder.equal(root.get("id"), id));

		List<T> r = session.createQuery(query).getResultList();
		if (!r.isEmpty()) {
			return r.get(0);
		}
		return null;
	}

}
