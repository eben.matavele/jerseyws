# jerseyws

A Jersey Web Service API

Essa API implementa uma basica JWT Authorization.

Autorização é necessária para aceder os recursos.

## Usage

1. Crie uma bd com o nome '2ibi' num SGDB MySQL 
2.  Empacote e coloque o ws.war num tomcat10
3. Instale a extenção do navegador Chrome "Talend API Tester" e teste a API.

### Autorização
Para se aceder a API é necessário obter o token de autorização JWT. 

Deverá colocar o token no HEADER "Authorization" para obter a autorização de acesso aos recursos da API.

Efecture o seguinte POST para obter o token:

```
METHOD
POST

LINK
http://localhost:8080/ws/auth

BODY 
username:admin, passoword:admin

FORM ENCODING 
application/x-www-form-urlencoded
```
### Buscar todos paises

```
METHOD
GET

LINK
http://localhost:8080/ws/paises/getAll

HEADERS
Content-Type: application/x-www-form-urlencoded
Authorization: token obtido
```
### Buscar todos paises ordenados

```
METHOD
GET

LINK
http://localhost:8080/ws/paises/getAllSorted?attributeName=code

O attributeName no link pode ser code, id, name, ...

HEADERS
Content-Type: application/x-www-form-urlencoded
Authorization: token obtido
```
### Remover pais

```
METHOD
REMOVE

LINK
http://localhost:8080/ws/paises/remover/2

O ultimo parámetro e o id do pais

HEADERS
Content-Type: application/x-www-form-urlencoded
Authorization: token obtido
```
### Adicionar pais
```

METHOD
POST

LINK
http://localhost:8080/ws/paises/add

HEADERS
Content-Type: application/x-www-form-urlencoded
Authorization: token obtido

BODY
name: nome do pais
code: codigo (MZ)
capital: capita do pais
...

O name e code são obrigatórios

FORM ENCODING 
application/x-www-form-urlencoded
```

### Editar pais

```
METHOD
PUT

LINK
http://localhost:8080/ws/paises/update

HEADERS
Content-Type: application/x-www-form-urlencoded
Authorization: token obtido

BODY
id:id do pais
name: nome do pais
code: codigo (MZ)
capital: capita do pais
...

O id do pais é obrigatório

FORM ENCODING 
application/x-www-form-urlencoded
```
## Autor

Eben Matavele | eben.matavele@gmail.com.
