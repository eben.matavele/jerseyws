/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MariaDB
 Source Server Version : 100505
 Source Host           : localhost:3306
 Source Schema         : 2ibi

 Target Server Type    : MariaDB
 Target Server Version : 100505
 File Encoding         : 65001

 Date: 04/02/2022 18:13:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (4);

-- ----------------------------
-- Table structure for pais
-- ----------------------------
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `code` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `capital` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `area` int(11) NULL DEFAULT NULL,
  `subregiao` int(11) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `subregiao`(`subregiao`) USING BTREE,
  CONSTRAINT `pais_ibfk_1` FOREIGN KEY (`subregiao`) REFERENCES `sub_regiao` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pais
-- ----------------------------
INSERT INTO `pais` VALUES (1, 'Moçambique', 'MZ', 'Cidade de Maputo', 801590, 2, 0, '2022-01-27 01:06:22', NULL, NULL);
INSERT INTO `pais` VALUES (3, NULL, 'AA', NULL, NULL, NULL, NULL, '2022-01-27 01:17:19', NULL, NULL);

-- ----------------------------
-- Table structure for regiao
-- ----------------------------
DROP TABLE IF EXISTS `regiao`;
CREATE TABLE `regiao`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designacao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of regiao
-- ----------------------------
INSERT INTO `regiao` VALUES (1, 'Asia', 0, NULL, NULL, NULL);
INSERT INTO `regiao` VALUES (2, 'Africa', 0, NULL, NULL, NULL);
INSERT INTO `regiao` VALUES (3, 'Europe', 0, NULL, NULL, NULL);
INSERT INTO `regiao` VALUES (4, '	North America', 0, NULL, NULL, NULL);
INSERT INTO `regiao` VALUES (5, 'South America', 0, NULL, NULL, NULL);
INSERT INTO `regiao` VALUES (6, 'Australia/Oceania', 0, NULL, NULL, NULL);
INSERT INTO `regiao` VALUES (7, 'Antarctica', 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sub_regiao
-- ----------------------------
DROP TABLE IF EXISTS `sub_regiao`;
CREATE TABLE `sub_regiao`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designacao` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `regiao` int(11) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `regiao`(`regiao`) USING BTREE,
  CONSTRAINT `sub_regiao_ibfk_1` FOREIGN KEY (`regiao`) REFERENCES `regiao` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sub_regiao
-- ----------------------------
INSERT INTO `sub_regiao` VALUES (1, 'África do Norte', 2, 0, '2022-01-26 17:47:06', NULL, NULL);
INSERT INTO `sub_regiao` VALUES (2, 'África Oriental', 2, 0, '2022-01-26 18:55:38', NULL, NULL);
INSERT INTO `sub_regiao` VALUES (3, 'África Central', 2, 0, '2022-01-26 17:47:15', NULL, NULL);
INSERT INTO `sub_regiao` VALUES (4, 'África Ocidental', 2, 0, '2022-01-26 17:47:15', NULL, NULL);
INSERT INTO `sub_regiao` VALUES (5, 'África do Sul', 2, 0, '2022-01-26 17:47:15', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
